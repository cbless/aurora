from flask import Flask, render_template, url_for
from flask_bootstrap import Bootstrap
from flask_migrate import Migrate
from flask_security import Security, SQLAlchemyUserDatastore
from flask_debugtoolbar import DebugToolbarExtension
from flask_ckeditor import CKEditor

from .core.models.accounts import  Role, User
from .core.db import db
from .auth.forms import ExtendedLoginForm
from .admin import configure_admin

import os

ckeditor = CKEditor()
bootstrap = Bootstrap()
migrate = Migrate()
toolbar = DebugToolbarExtension()


def create_app(config_class):
    app = Flask(__name__)
    app.config.from_object(config_class)

    _dir = os.path.dirname(os.path.abspath(__file__))
    app.template_folder = os.path.join(_dir, "web/templates")
    app.static_folder = os.path.join(_dir, "web/static")

    db.init_app(app)
    migrate.init_app(app)

    # initialize extensions
    bootstrap.init_app(app)
    ckeditor.init_app(app)
    #if config_class.DEBUG:
    #    toolbar.init_app(app)

    # security
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    app.security = Security(app, user_datastore)

    # import blueprints
    register_blueprints(app)
    # register and configure flask-admin extension
    configure_admin(app, db)

    return app


def register_blueprints(app):
    from .home import home as home_bp
    app.register_blueprint(home_bp)

    from .contacts import contact_bp
    app.register_blueprint(contact_bp)

    from .projects import project_bp
    app.register_blueprint(project_bp)

    #from .profile import profile as profile_bp
    #app.register_blueprint(profile_bp)
    # from .api import api_bp
    # app.register_blueprint(api_bp)


def register_errorhandlers(app):
    """Register error handlers with the Flask application."""
    @app.errorhandler(403)
    def forbidden(error):
        return render_template('errors/403.html', title='Forbidden'), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('errors/404.html', title='Page Not Found'), 404

    @app.errorhandler(500)
    def internal_server_error(error):
        return render_template('errors/500.html', title='Server Error'), 500
