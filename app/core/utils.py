from .db import db
from .models.accounts import User
from .models.projects import Project
from .models.vulnerabilities import Tag, Reference
from .vars import ADMIN_ROLE_NAME


def has_access(pid, user):
    projects = [ p.id for p in find_projects_for_user(user)]

    if pid in projects:
        return True

    return False


def find_projects_for_user(user):
    # an admin can see everything
    if user.has_role(ADMIN_ROLE_NAME):
        return Project.query.all()

    # a user can either be an project manager or an assessor of the project
    return Project.query.filter((Project.project_manager_id==user.id) | (Project.assessors.any(id=user.id))).all()


def find_pm_projects(user):
    projects = Project.query.filter( Project.project_manager_id==user.id).all()
    return projects


def find_assessor_projects(user):
    projects = Project.query.filter(Project.assessors.any(id=user.id)).all()
    return projects


def find_users_by_project(project):
    project_users = [ u for u in project.assessors]
    project_users.append(project.project_manager)
    return project_users


def get_tag_choices():
    choices = [(t.id, t.name) for t in Tag.query.all()]
    return choices


def get_reference_choices():
    choices = [(r.id, r.name) for r in Reference.query.all()]
    return choices
