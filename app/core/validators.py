from wtforms.validators import ValidationError, Regexp


class NameValidator(Regexp):
    def __init__(self, message=None):
        if not message:
            message = "Only letters, numbers and the special characters _ or - and . are allowed"
        Regexp.__init__(self,"^(\w|[-_ \.])+$",message=message)


