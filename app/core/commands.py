from flask_script import Command, Option
from flask_security.utils import hash_password

from .db import db
from .models.accounts import Role, User
from .models.core import ImpactLevel, LikelihoodLevel, Status
from .vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME


class InitDB(Command):
    def run(self):
        admin_role = Role(name=ADMIN_ROLE_NAME, description='Privileged Role for administrative tasks')
        assessor_role = Role(name=ASSESSOR_ROLE_NAME, description='Role with limited access')
        pm_role = Role(name=PM_ROLE_NAME, description='Project manager role')

        admin_user = User(email='admin@test.local', username='admin', password=hash_password('admin'),
                          active=True, roles=[admin_role])
        assessor1_user = User(email='assessor1@test.local', username='assessor1',
                              password=hash_password('assessor1'),
                              active=True, roles=[assessor_role, pm_role])
        assessor2_user = User(email='assessor2@test.local', username='assessor2',
                              password=hash_password('assessor2'),
                              active=True, roles=[assessor_role])
        assessor3_user = User(email='assessor3@test.local', username='assessor3',
                              password=hash_password('assessor3'),
                              active=True, roles=[assessor_role])
        pm_user = User(email='pm1@test.local', username='pm1', password=hash_password('pm1'), active=True,
                       roles=[pm_role])

        db.session.add(admin_role)
        db.session.add(assessor_role)
        db.session.add(pm_role)

        db.session.add(admin_user)
        db.session.add(assessor1_user)
        db.session.add(assessor2_user)
        db.session.add(assessor3_user)
        db.session.add(pm_role)

        db.session.add(ImpactLevel(name="Critical"))
        db.session.add(ImpactLevel(name='High'))
        db.session.add(ImpactLevel(name='Medium'))
        db.session.add(ImpactLevel(name='Low'))
        db.session.add(ImpactLevel(name='Info'))

        db.session.add(LikelihoodLevel(name="Critical"))
        db.session.add(LikelihoodLevel(name='High'))
        db.session.add(LikelihoodLevel(name='Medium'))
        db.session.add(LikelihoodLevel(name='Low'))
        db.session.add(LikelihoodLevel(name='Info'))

        db.session.add(Status(name='open'))
        db.session.add(Status(name='in_progress'))
        db.session.add(Status(name='closed'))
        db.session.add(Status(name='on_hold'))
        db.session.add(Status(name='cancelled'))
        db.session.add(Status(name='done'))

        db.session.commit()
