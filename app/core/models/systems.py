from ..db import db


class SystemType(db.Model):
    __tablename__ = "system_types"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text())

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class System(db.Model):
    __tablename__ = "systems"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)

    system_type_id = db.Column(db.Integer, db.ForeignKey('system_types.id'))
    system_type = db.relationship("SystemType")

    address = db.Column(db.String(2048), nullable=True)
    description = db.Column(db.Text())

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'))
    project = db.relationship('Project')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class Host(db.Model):
    __tablename__ = "hosts"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    hostname = db.Column(db.String(255), nullable=True)
    ipv4 = db.Column(db.String(18), nullable=True)
    ipv6 = db.Column(db.String(100), nullable=True)
    os = db.Column(db.String(2048), nullable=True)

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'))
    project = db.relationship('Project')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)