from ..db import db

vulnerability_references = db.Table('vulnerability_references',
                                   db.Column('ref_id', db.Integer, db.ForeignKey('references.id')),
                                   db.Column('vulnerability_id', db.Integer, db.ForeignKey('vulnerabilities.id')))


vulnerability_tags = db.Table('vulnerability_tags',
                             db.Column('tag_id', db.Integer, db.ForeignKey('tags.id')),
                             db.Column('vulnerability_id', db.Integer, db.ForeignKey('vulnerabilities.id')))

vulnerability_hosts = db.Table('vulnerability_hosts',
                             db.Column('host_id', db.Integer, db.ForeignKey('hosts.id')),
                             db.Column('vulnerability_id', db.Integer, db.ForeignKey('vulnerabilities.id')))


class Tag(db.Model):
    __tablename__ = "tags"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Tag' when saving other objects
    def __hash__(self):
        return hash(self.name)


class Reference(db.Model):
    __tablename__ = "references"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    link = db.Column(db.String(2048), nullable=False)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Reference' when saving other objects
    def __hash__(self):
        return hash(self.name)

class Vulnerability(db.Model):
    __tablename__ = "vulnerabilities"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    url = db.Column(db.String(2048), nullable=True)
    description = db.Column(db.Text())
    solution = db.Column(db.Text())

    impact_id = db.Column(db.Integer, db.ForeignKey('impactlevel.id'), nullable=False)
    impact = db.relationship('ImpactLevel', backref="Vulnerability impact")

    likelihood_id = db.Column(db.Integer, db.ForeignKey('likelihoodlevel.id'), nullable=False)
    likelihood = db.relationship('LikelihoodLevel', backref="Vulnerability likelihood")

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=False)
    project = db.relationship('Project', backref=db.backref("vuln_by_project"))

    hosts = db.relationship(
        'Host',
        secondary=vulnerability_hosts,
        backref=db.backref('vulnerabilities', lazy='dynamic')
    )

    tags = db.relationship(
        'Tag',
        secondary=vulnerability_tags
    )
    references = db.relationship(
        'Reference',
        secondary=vulnerability_references
    )

    exploit_available = db.Column(db.Boolean)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

