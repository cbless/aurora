
from ..db import db


defaultvulns_references_association = db.Table('defaultvulns_references',
                                   db.Column('ref_id', db.Integer, db.ForeignKey('references.id')),
                                   db.Column('defaultvuln_id', db.Integer, db.ForeignKey('defaultvulns.id')))


defaultvulns_tags_association = db.Table('defaultvulns_tags',
                             db.Column('tag_id', db.Integer, db.ForeignKey('tags.id')),
                             db.Column('defaultvuln_id', db.Integer, db.ForeignKey('defaultvulns.id')))


class DefaultVuln(db.Model):
    __tablename__ = "defaultvulns"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    description = db.Column(db.Text())
    solution = db.Column(db.Text())

    impact_id = db.Column(db.Integer, db.ForeignKey('impactlevel.id'))
    impact = db.relationship('ImpactLevel')

    likelihood_id = db.Column(db.Integer, db.ForeignKey('likelihoodlevel.id'))
    likelihood = db.relationship('LikelihoodLevel')

    tags = db.relationship(
        'Tag',
        secondary=defaultvulns_tags_association,
        backref=db.backref('tagsDefaultVuln', lazy='dynamic')
    )
    references = db.relationship(
        'Reference',
        secondary=defaultvulns_references_association,
        backref=db.backref('referencesDefaultVuln', lazy='dynamic')
    )
    exploit_available = db.Column(db.Boolean())
