from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ..core.models import User
from ..core.db import db

from . import profile


@profile.route('/', methods=['GET', 'POST'])
@login_required
def profile():
    user = current_user

    if user is None:
        flash("Error: Can't modify user", 'error')
        return redirect(url_for('home.index'))
    else:
        flash("TODO: implement this feature", 'success')
        return redirect(url_for('home.index'))