from flask import Blueprint

contact_bp = Blueprint('contacts', __name__, url_prefix='/contacts', template_folder='templates')

from .views.companies import *
from .views.contacts import *
from .views.profile import *