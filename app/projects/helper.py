from ..core.models.projects import Project, project_methodologies_association, Module, project_assessors_association
from ..core.models.methodologies import Methodology
from ..core.models.systems import System, Host
from ..core.models.accounts import User


def get_modules_for_project(pid):
    return Module.query.filter(Module.project_id == pid).all()


def get_methodologies_for_project(pid):
    return Methodology.query.join(project_methodologies_association).join(Project).filter(Project.id == pid).all()


def get_systems_for_project(pid):
    return System.query.filter(System.project_id == pid).all()


def get_hosts_for_project(pid):
    return Host.query.filter(Host.project_id == pid).all()


def get_assessors_for_project(pid):
    return User.query.join(project_assessors_association).join(Project).filter(Project.id == pid).all()


def get_pm_for_project(pid):
    p = Project.query.get_or_404(pid)
    return p.project_manager


def get_project_members(pid):
    p = Project.query.get_or_404(pid)
    return get_members_from_project(p)


def get_members_from_project(project):
    results = [ a for a in project.assessors]
    results.append(project.project_manager)
    return results