from flask import abort, flash, redirect, render_template, request, url_for
from flask_security import login_required, roles_accepted, current_user

from ...core.db import db
from ...core.vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...core.models.projects import Project
from ...core.models.systems import Host
from ...core.utils import has_access

from .. import project_bp
from ..forms import HostForm
from ..helper import get_hosts_for_project


@project_bp.route('/<int:pid>/hosts/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def hosts(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    hosts = get_hosts_for_project(pid)

    return render_template('hosts/hostlist.html', project=project, hosts=hosts, title="List of hosts")


@project_bp.route('/<int:pid>/hosts/add', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def add_host(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)

    form = HostForm()

    if request.method == 'POST' and form.validate_on_submit():
        host = Host(name=form.name.data,
                    hostname=form.hostname.data,
                    ipv4=form.ipv4.data,
                    ipv6=form.ipv6.data,
                    os=form.os.data,
                    project=project
                    )
        try:
            db.session.add(host)
            db.session.commit()
            flash('You have successfully added a new Host.', 'success')
        except:
            flash('Error: Hostname already exists.', 'error')

        return redirect(url_for('projects.hosts', pid=project.id))

    return render_template('hosts/host.html', form=form, project=project, title="Add Host")



@project_bp.route('/<int:pid>/hosts/edit/<int:id>', methods=['GET', 'POST'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def edit_host(pid, id):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    host = Host.query.get_or_404(id)
    form = HostForm()
    form.project.data = project
    if form.validate_on_submit():
        form.populate_obj(host)
        try:
            db.session.add(host)
            db.session.commit()
            flash('You have successfully modified a host.', 'success')
        except:
            flash("Error: Something went wrong, host wasn't modified.", 'error')

        return redirect(url_for('projects.hosts', pid=project.id))

    form.name.data = host.name
    form.hostname.data = host.hostname
    form.ipv4.data = host.ipv4
    form.ipv6.data = host.ipv6
    form.os.data = host.os
    form.project.data = host.project_id

    return render_template('hosts/host.html', project=project, form=form, title="Modify Host")