from flask import abort, flash, redirect, render_template, request, url_for, Response
from flask_security import login_required, roles_accepted, current_user
from docxtpl import DocxTemplate, RichText
from lxml import etree
import os

from ...core.vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME
from ...core.models.projects import Project, Todo, Module
from ...core.models.vulnerabilities import Vulnerability
from ...core.models.systems import System, Host
from ...core.utils import has_access
from .. import project_bp

from io import StringIO, BytesIO ## for Python 3

import jinja2
from jinja2 import Template

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__),"../../.."))
template_dir = "{0}/reports/templates/".format(basedir)
report_dir = "{0}/reports/outdir/".format(basedir)

latex_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%%',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(basedir)
)


@project_bp.route('/<int:pid>/export/', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def list_templates(pid):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)
    templates = os.listdir(template_dir)
    return render_template('export/templatelist.html', project=project, templates=templates,
                           title="Available templates")


def generate_tex(template, current_user=None, project=None, modules=[], vulns=[], methodologies=[], systems=[],
                 hosts=[], todos=[],):
    document = latex_jinja_env.get_template(template)
    output = document.render(current_user=current_user, project=project, modules=modules,
                             vulns=vulns, methodologies=methodologies, systems=systems, hosts=hosts,
                             todos=todos, )
    return output

html_escape_table = {
     "&": "&amp;",
     '"': "&quot;",
     "'": "&apos;",
     ">": "&gt;",
     "<": "&lt;",
     }

def html_escape(text):
     """Produce entities within text."""
     return "".join(html_escape_table.get(c,c) for c in text)

def text_to_docx(text):
    try:
        et = etree.fromstring(text)
        print ( et)
        for p in et.par
        return RichText(html_escape(text))
    except:
        return text

def generate_docx(template, current_user=None, project=None, modules=[], vulns=[], methodologies=[], systems=[],
                 hosts=[], todos=[],):
    doc = DocxTemplate(template)
    print (project.description)
    print (project.summary)
    context = {'project': project, 'project_description': text_to_docx(project.description),
               'project_summary': text_to_docx(project.summary), 'current_user': current_user,
               'systems': systems, 'hosts':hosts, 'todos':todos,
               'modules': modules, 'vulns': vulns, 'methodologies' : methodologies}
    doc.render(context)
    f = BytesIO()
    doc.save(f)
    length = f.tell()
    f.seek(0)
    return f


@project_bp.route('/<int:pid>/export/<template>', methods=['GET'])
@login_required
@roles_accepted(ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME, PM_ROLE_NAME)
def export(pid, template):
    if not has_access(pid, current_user):
        abort(403, "You don't have access to the project.")
    project = Project.query.get_or_404(pid)

    if template not in os.listdir(template_dir):
        abort(403, "Unknown template.")
    template_file = "{0}{1}".format("reports/templates/", template)

    modules = Module.query.filter(Module.project_id == project.id).all()
    vulns = Vulnerability.query.filter(Vulnerability.project_id == project.id).all()
    todos = Todo.query.filter(Todo.project_id == project.id).all()
    methodologies = project.methodologies
    systems = System.query.filter(System.project_id == project.id).all()
    hosts = Host.query.filter(Host.project_id == project.id).all()

    project_name = project.name.replace(" ", "")

    if template_file[-4:] == ".tex":
        output = generate_tex(template_file, current_user=current_user, project=project, modules=modules, vulns=vulns,
                              methodologies=methodologies, systems=systems, hosts=hosts, todos=todos, )
        return Response(output, mimetype="text/tex",
                        headers={"Content-disposition": "attachment; filename={0}.tex".format(project_name)})
    elif template_file[-5:] == ".docx":
        output = generate_docx(template_file, current_user=current_user, project=project, modules=modules, vulns=vulns,
                              methodologies=methodologies, systems=systems, hosts=hosts, todos=todos, )
        return Response(output, mimetype="text/docx",
                        headers={"Content-disposition": "attachment; filename={0}.docx".format(project_name)})

    return redirect(url_for('projects.list_templates', pid=project.id))

