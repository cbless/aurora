from flask import Blueprint

project_bp = Blueprint('projects', __name__, url_prefix='/projects', template_folder='templates')

from .views.dashboard import *
from .views.project import *
from .views.vulns import *
from .views.modules import *
from .views.methodologies import *
from .views.todos import *
from .views.systems import *
from .views.hosts import *
from .views.export import *